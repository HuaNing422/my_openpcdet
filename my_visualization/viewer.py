import os
import numpy as np 
import json
from visual_tools import draw_clouds_with_boxes
import open3d as o3d

def dataloader(cloud_path , boxes_path):
    def json_to_np(path):
        labels = []
        boxes = []
        with open(path, 'r') as load_f:
            objects = json.load(load_f)
            for i in range(len(objects)):
                labels.append(objects[i]['label'])
                box = objects[i]
                x, y, z = box['xyz'][:]
                l, w, h = box['lwh'][:]
                yaw = box['yaw']
                boxes.append([x, y, z, l, w, h, yaw])
        return labels, np.array(boxes)
    if cloud_path[-4:] == '.bin':
        cloud = np.fromfile(cloud_path, dtype=np.float32).reshape(-1, 4)
    elif cloud_path[-4:] == '.pcd':
        pcd = o3d.io.read_point_cloud(cloud_path)
        cloud = np.asarray(pcd.points)
    else:
        raise ValueError
    labels, boxes = json_to_np(boxes_path)
    boxes = np.array(boxes)
    return cloud , boxes, labels

def dataloader_test(boxes_path, cloud_file):
    cloud = np.fromfile(cloud_file, dtype=np.float32).reshape(-1, 4)
    boxes = []
    labels = []
    with open(boxes_path, 'r', encoding='utf8') as f:
        lines = f.readlines()
        for line in lines:
            words = line.split(" ")
            words[-1] = words[-1][:-1]
            boxes.append(words[1:-1])
            labels.append(words[0])
    boxes = np.array(boxes, dtype=np.float32)
    return cloud , boxes, labels


if __name__ == "__main__":

    index = '005147'
    # boxes_path = r'results\box.txt'
    boxes_path = os.path.join('results', index+'.json')
    cloud_path = os.path.join('results', index+'.bin')
    if 'box' in boxes_path:
        print('PRED')
        cloud , boxes, labels = dataloader_test(cloud_path , boxes_path)
    else:
        print('True')
        cloud , boxes, labels = dataloader(cloud_path , boxes_path)
    draw_clouds_with_boxes(cloud, boxes, labels, True)