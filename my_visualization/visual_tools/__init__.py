import open3d as o3d
import numpy as np
from PIL import Image, ImageFont, ImageDraw
from pyquaternion import Quaternion
from .open3d_coordinate import create_coordinate
from .open3d_arrow import create_arrow
from .open3d_box import create_box

def text_3d(text, pos, direction=None, degree=0.0, font='C:/Windows/Fonts/Arial.ttf', font_size=80):
    """
    Generate a 3D text point cloud used for visualization.
    :param text: content of the text
    :param pos: 3D xyz position of the text upper left corner
    :param direction: 3D normalized direction of where the text faces
    :param degree: in plane rotation of text
    :param font: Name of the font - change it according to your system
    :param font_size: size of the font
    :return: o3d.geoemtry.PointCloud object
    """
    if direction is None:
        direction = (0., 0., 1.)

    font_obj = ImageFont.truetype(font, font_size)
    font_dim = font_obj.getsize(text)

    img = Image.new('RGB', font_dim, color=(255, 255, 255))
    draw = ImageDraw.Draw(img)
    draw.text((0, 0), text, font=font_obj, fill=(0, 0, 0))
    img = np.asarray(img)
    img_mask = img[:, :, 0] < 128
    indices = np.indices([*img.shape[0:2], 1])[:, img_mask, 0].reshape(3, -1).T

    pcd = o3d.geometry.PointCloud()
    pcd.colors = o3d.utility.Vector3dVector(img[img_mask, :].astype(float) / 255.0)
    pcd.points = o3d.utility.Vector3dVector(indices / 100.0)

    raxis = np.cross([0.0, 0.0, 1.0], direction)
    if np.linalg.norm(raxis) < 1e-6:
        raxis = (0.0, 0.0, 1.0)
    trans = (Quaternion(axis=raxis, radians=np.arccos(direction[2])) *
             Quaternion(axis=direction, degrees=degree)).transformation_matrix
    trans[0:3, 3] = np.asarray(pos)
    pcd.transform(trans)
    return pcd

def create_box_with_arrow(box, color=None):
    """
    box: list(8) [ x, y, z, dx, dy, dz, yaw]
    """

    box_o3d = create_box(box, color)
    x = box[0]
    y = box[1]
    z = box[2]
    l = box[3]
    yaw = box[6]
    # get direction arrow
    dir_x = l / 2.0 * np.cos(yaw)
    dir_y = l / 2.0 * np.sin(yaw)

    arrow_origin = [x - dir_x, y - dir_y, z]
    arrow_end = [x + dir_x, y + dir_y, z]
    arrow = create_arrow(arrow_origin, arrow_end, color)

    return box_o3d, arrow


def draw_clouds_with_boxes(cloud , boxes, labels=None, draw_label=False):
    """
    cloud: (N, 4)  [x, y, z, intensity]
    boxes: (n,7) np.array = n*7  ( x, y, z, dx, dy, dz, yaw) 
    """
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    # --------------------------------------------------------------
    # create point cloud
    # --------------------------------------------------------------
    points_color = [[0.5, 0.5, 0.5]]  * cloud.shape[0]
    pc = o3d.geometry.PointCloud()
    pc.points = o3d.utility.Vector3dVector(cloud[:,:3])
    pc.colors = o3d.utility.Vector3dVector(points_color)
    vis.add_geometry(pc)
    # --------------------------------------------------------------
    # create boxes with colors with arrow
    # --------------------------------------------------------------
    boxes_o3d = []

    color_dict = {'vehicle': [1, 0, 0], 'pedestrian': [0, 1, 0], 'bicycle': [0, 0, 1], \
                  'large_vehicle': [0.5, 0, 0.5], 'small_lowvel_vehicle': [0.5, 0.5, 0], \
                  'huge_vehicle': [0, 0.5, 0.5]}

    # create boxes
    for i in range(len(boxes)):
        box = boxes[i]
        box_o3d, arrow = create_box_with_arrow(box, color_dict[labels[i]])
        boxes_o3d.append(box_o3d)
        boxes_o3d.append(arrow)
        if draw_label:
            pos_label = box[:3]
            pos_label[-1] += box[5]/2
            pc_label = text_3d(str(round(np.sqrt(box[0]**2+box[1]**2+box[2]**2), 1)), pos_label)
            vis.add_geometry(pc_label)
    # add_geometry fro boxes
    [vis.add_geometry(element) for element in boxes_o3d]

    # --------------------------------------------------------------
    # coordinate frame
    # --------------------------------------------------------------
    coordinate_frame = create_coordinate(size=2.0, origin=[0, 0, 0])
    vis.add_geometry(coordinate_frame)

    # --------------------------------------------------------------
    # drop the window
    # --------------------------------------------------------------
    vis.get_render_option().point_size = 2
    vis.run()
    vis.destroy_window()
