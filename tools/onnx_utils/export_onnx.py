import argparse
from pathlib import Path
import os
import onnxruntime
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import datetime
from pcdet.config import cfg, cfg_from_yaml_file
from pcdet.models.backbones_3d.vfe.pillar_vfe import PFNLayer
from pcdet.models.backbones_3d.vfe.vfe_template import VFETemplate
from onnx_backbone_2d import BaseBEVBackbone
from onnx_anchor_head import AnchorHeadMulti
from onnx_center_head import CenterHead

def parse_config():
    parser = argparse.ArgumentParser(description='arg parser')
    parser.add_argument('--cfg_file', type=str, default='/home/hning/lidar/my_openpcdet/tools/cfgs/zhisuo_models/centerpoint.yaml',
                        help='specify the config for demo')
    parser.add_argument('--onnx_path', type=str, default='/home/hning/lidar/my_openpcdet/output/onnx_files',
                        help='specify the point cloud data file or directory')
    parser.add_argument('--ckpt', type=str, default='/home/hning/lidar/my_openpcdet/output/zhisuo_models/centerpoint/default/ckpt/checkpoint_epoch_80.pth', help='specify the pretrained model')
    parser.add_argument('--head_type', type=str, default='center', help='specify the head type')
    parser.add_argument('--need_check', type=bool, default=True, help='check the outputs of onnx models')

    args = parser.parse_args()

    cfg_from_yaml_file(args.cfg_file, cfg)

    return args, cfg


class PillarVFE(VFETemplate):
    def __init__(self, model_cfg, num_point_features, voxel_size, point_cloud_range, **kwargs):
        super().__init__(model_cfg=model_cfg)

        self.use_norm = self.model_cfg.USE_NORM
        self.with_distance = self.model_cfg.WITH_DISTANCE
        self.use_absolute_xyz = self.model_cfg.USE_ABSLOTE_XYZ
        num_point_features += 6 if self.use_absolute_xyz else 3
        if self.with_distance:
            num_point_features += 1

        self.num_filters = self.model_cfg.NUM_FILTERS
        assert len(self.num_filters) > 0
        num_filters = [num_point_features] + list(self.num_filters)

        pfn_layers = []
        for i in range(len(num_filters) - 1):
            in_filters = num_filters[i]
            out_filters = num_filters[i + 1]
            pfn_layers.append(
                PFNLayer(in_filters, out_filters, self.use_norm, last_layer=(i >= len(num_filters) - 2))
            )
        self.pfn_layers = nn.ModuleList(pfn_layers)

        self.voxel_x = voxel_size[0]
        self.voxel_y = voxel_size[1]
        self.voxel_z = voxel_size[2]
        self.x_offset = self.voxel_x / 2 + point_cloud_range[0]
        self.y_offset = self.voxel_y / 2 + point_cloud_range[1]
        self.z_offset = self.voxel_z / 2 + point_cloud_range[2]

    def forward(self, features, **kwargs):
        for pfn in self.pfn_layers:
            features = pfn(features)
        features = features # .squeeze()
        return features

    def load_params_from_file(self, filename, to_cpu=True):
        if not os.path.isfile(filename):
            raise FileNotFoundError

        print('==> Loading parameters from checkpoint %s to %s' % (filename, 'CPU' if to_cpu else 'GPU'))
        loc_type = torch.device('cpu') if to_cpu else None
        checkpoint = torch.load(filename, map_location=loc_type)
        model_state_disk = checkpoint['model_state']

        if 'version' in checkpoint:
            print('==> Checkpoint trained from version: %s' % checkpoint['version'])

        update_model_state = {}
        for key, val in model_state_disk.items():
            temp_key = key[4:]
            if temp_key in self.state_dict() and self.state_dict()[temp_key].shape == model_state_disk[key].shape:
                update_model_state[temp_key] = val
                # logger.info('Update weight %s: %s' % (key, str(val.shape)))

        state_dict = self.state_dict()
        state_dict.update(update_model_state)
        self.load_state_dict(state_dict)

        for key in state_dict:
            if key not in update_model_state:
                print('Not updated weight %s: %s' % (key, str(state_dict[key].shape)))

        print('==> Done (loaded %d/%d)' % (len(update_model_state), len(self.state_dict())))

class Head(nn.Module):
    def __init__(self, cfg, gridx, gridy, head_type='center'):
        super().__init__()
        self.head_type = head_type
        self.backbone_2d = BaseBEVBackbone(cfg.MODEL.BACKBONE_2D, 64)
        if head_type == 'anchor':
            self.dense_head =  AnchorHeadMulti(
                model_cfg=cfg.MODEL.DENSE_HEAD,
                input_channels=384,
                num_class=len(cfg.CLASS_NAMES),
                class_names=cfg.CLASS_NAMES,
                grid_size=np.array([gridx , gridy , 1]),
                point_cloud_range=cfg.DATA_CONFIG.POINT_CLOUD_RANGE,
                predict_boxes_when_training=False)
        elif head_type == 'center':
            self.dense_head =  CenterHead(
                model_cfg=cfg.MODEL.DENSE_HEAD,
                input_channels=384,
                num_class=len(cfg.CLASS_NAMES),
                class_names=cfg.CLASS_NAMES,
                grid_size=np.array([gridx , gridy , 1]),
                point_cloud_range=cfg.DATA_CONFIG.POINT_CLOUD_RANGE,
                voxel_size=cfg.DATA_CONFIG.DATA_PROCESSOR[2]['VOXEL_SIZE'],
                predict_boxes_when_training=False)
        else:
            raise ValueError

    def forward(self, spatial_features):
        x = self.backbone_2d(spatial_features)
        if self.head_type == 'anchor':
            batch_cls_preds, batch_box_preds = self.dense_head.forward(x)
            return batch_cls_preds, batch_box_preds
        elif self.head_type == 'center':
            preds = self.dense_head.forward(x)
            # for task in range(len(preds)):
            #     hm_preds = torch.sigmoid(preds[task]['hm'])
            #     preds[task]['dim'] = torch.exp(preds[task]['dim'])
            #     scores, labels = torch.max(hm_preds, dim=1)
            #     preds[task]["hm"] = (scores, labels)
            # return preds[0]['hm'], preds[0]['center'], preds[0]['center_z'], preds[0]['dim'], preds[0]['rot']
            return preds
        else:
            raise ValueError
    
    def load_params_from_file(self, filename, to_cpu=True):
        if not os.path.isfile(filename):
            raise FileNotFoundError

        print('==> Loading parameters from checkpoint %s to %s' % (filename, 'CPU' if to_cpu else 'GPU'))
        loc_type = torch.device('cpu') if to_cpu else None
        checkpoint = torch.load(filename, map_location=loc_type)
        model_state_disk = checkpoint['model_state']

        if 'version' in checkpoint:
            print('==> Checkpoint trained from version: %s' % checkpoint['version'])

        update_model_state = {}
        for key, val in model_state_disk.items():
            if key in self.state_dict() and self.state_dict()[key].shape == model_state_disk[key].shape:
                update_model_state[key] = val
                # logger.info('Update weight %s: %s' % (key, str(val.shape)))

        state_dict = self.state_dict()
        state_dict.update(update_model_state)
        self.load_state_dict(state_dict)

        for key in state_dict:
            if key not in update_model_state:
                print('Not updated weight %s: %s' % (key, str(state_dict[key].shape)))

        print('==> Done (loaded %d/%d)' % (len(update_model_state), len(self.state_dict())))

def generate_input_and_model(cfg, args):
    max_num_pillars = cfg.DATA_CONFIG.DATA_PROCESSOR[2].MAX_NUMBER_OF_VOXELS['test']
    max_points_per_pillars = cfg.DATA_CONFIG.DATA_PROCESSOR[2].MAX_POINTS_PER_VOXEL
    dims_feature = cfg.DATA_CONFIG.DATA_AUGMENTOR.AUG_CONFIG_LIST[0].NUM_POINT_FEATURES \
                   + 6 if cfg.MODEL.VFE.USE_ABSLOTE_XYZ else 3
    dummy_pc = torch.randn(max_num_pillars,max_points_per_pillars,dims_feature).cuda()
    pc_range = np.array(cfg.DATA_CONFIG.POINT_CLOUD_RANGE)
    voxel_size = np.array(cfg.DATA_CONFIG.DATA_PROCESSOR[2]['VOXEL_SIZE'])
    grid_size = (pc_range[3:] - pc_range[:3]) /voxel_size
    gridx = grid_size[0].astype(np.int)
    gridy = grid_size[1].astype(np.int)
    dummy_img = torch.randn(1, 64, gridx, gridy).cuda()

    pfe =PillarVFE(            
            model_cfg=cfg.MODEL.VFE,
            num_point_features=cfg.DATA_CONFIG.DATA_AUGMENTOR.AUG_CONFIG_LIST[0]['NUM_POINT_FEATURES'],
            point_cloud_range=cfg.DATA_CONFIG.POINT_CLOUD_RANGE,  
            voxel_size=cfg.DATA_CONFIG.DATA_PROCESSOR[2].VOXEL_SIZE)
    pfe.to('cuda').eval()
    pfe.load_params_from_file(args.ckpt)
    rpn = Head(cfg, gridx, gridy, args.head_type)
    rpn.to('cuda').eval()
    rpn.load_params_from_file(args.ckpt)

    return dummy_pc, dummy_img, pfe, rpn

def get_onnx_path(args):
    now = datetime.datetime.now()
    today = now.strftime('%y%m%d')
    onnx_pfe_path = os.path.join(args.onnx_path, args.head_type + '_pfe_' + today + '.onnx')
    onnx_rpn_path = os.path.join(args.onnx_path, args.head_type + '_rpn_' + today + '.onnx')
    return onnx_pfe_path, onnx_rpn_path

def onnx_model_predict(model_dir=None, input_test=None):

    # input_test = torch.ones([50000, 20, 10], dtype=torch.float32, device="cuda:0")
    onnx_session = onnxruntime.InferenceSession(model_dir)

    # Compute ONNX Runtime output prediction
    onnx_inputs = {onnx_session.get_inputs()[0].name: (input_test.data.cpu().numpy())}
    onnx_outs = onnx_session.run(None, onnx_inputs)
    return onnx_outs

def main():
    args, cfg = parse_config()
    dummy_pc, dummy_img, pfe, rpn = generate_input_and_model(cfg, args)
    onnx_pfe_path, onnx_rpn_path = get_onnx_path(args)

    with torch.no_grad():
        input_names = ["input_features"]
        output_names = ["pillar_features"]
        dynamic_axes= {'input_features': {0: 'num_voxels', 1: 'num_max_points'}, 'pillar_features': {0: 'num_voxels'}}
        torch.onnx.export(pfe,
                        dummy_pc,
                        onnx_pfe_path,
                        opset_version=11,
                        input_names=input_names, output_names=output_names, dynamic_axes=dynamic_axes
                        )
        print('Pfe done.')

        # getting errors with opset_version 11 or 12, changed with 10
        input_names = ["spatial_features"]
        ###### output_names = ["heatmap", "reg", "height", "dim", "rot"] # ["rot", "dim", "height", "reg", "heatmap"]
        output_names = ["reg", "height", "dim", "rot", "heatmap"]
        dynamic_axes= {'spatial_features': {0: 'batch_size', 2: 'H', 3: 'W'}, \
                       'reg': {0: 'batch_size', 2: 'H', 3: 'W'}, \
                       'height': {0: 'batch_size', 2: 'H', 3: 'W'}, \
                       'dim': {0: 'batch_size', 2: 'H', 3: 'W'}, \
                       'rot': {0: 'batch_size', 2: 'H', 3: 'W'}, \
                       'heatmap': {0: 'batch_size', 2: 'H', 3: 'W'}}
        torch.onnx.export(rpn,
                        dummy_img,
                        onnx_rpn_path,
                        opset_version=10,
                        input_names=input_names, output_names=output_names, dynamic_axes=dynamic_axes)
        print('Rpn done.')

        if args.need_check:
            # dummy_my = torch.from_numpy(np.load('/home/data/dataset/zhisuo_lidar_data/tmp/voxel_input.npy')).cuda().type_as(dummy_img)
            # dummy_wf = torch.from_numpy(np.load('/home/data/dataset/zhisuo_lidar_data/tmp/voxel_input_tensorrt.npy')).cuda().type_as(dummy_img)

            pfe_onnx = onnx_model_predict(onnx_pfe_path, dummy_pc)
            pfe_pytorch = pfe(dummy_pc)
            print('-------------------- PFE ONNX Outputs --------------------')
            print(pfe_onnx)
            print('-------------------- PFE Pytorch Outputs --------------------')
            print(pfe_pytorch)

            # def rel_l2(value_torch, value_rt):
            #     value_torch = value_torch.reshape(-1)
            #     value_rt = value_rt.reshape(-1)
            #     rel = np.abs((value_rt - value_torch) / (value_torch + 1e-12))
            #     return rel.mean()
            # print(rel_l2(pfe_onnx[0][:152, ...], pfe_pytorch.cpu().numpy()[:152, ...]))
            # np.save('middle_output.npy', pfe_onnx[0][None, ...])


            # dummy_my = torch.from_numpy(np.load('/home/data/dataset/zhisuo_lidar_data/tmp/middle_input.npy')).cuda().type_as(dummy_img)
            # dummy_wf = torch.from_numpy(np.load('/home/data/dataset/zhisuo_lidar_data/tmp/wf.npy')).cuda().type_as(dummy_img)
            rpn_onnx = onnx_model_predict(onnx_rpn_path, dummy_img)
            rpn_pytorch = rpn(dummy_img)
            print('-------------------- RPN ONNX Outputs --------------------')
            print(rpn_onnx)
            print('-------------------- RPN Pytorch Outputs --------------------')
            print(rpn_pytorch)

            # def rel_l2(value_torch, value_rt):
            #     value_torch = value_torch.reshape(-1)
            #     value_rt = value_rt.reshape(-1)
            #     rel = np.abs((value_rt - value_torch) / (value_torch + 1e-12))
            #     return rel.mean()
            # print('center: ', rel_l2(rpn_onnx[0], rpn_pytorch[0]['center'].cpu().numpy()))
            # # print('center: ', rel_l2(rpn_onnx[0]['center'].cpu().numpy(), rpn_pytorch[0]['center'].cpu().numpy()))
            # print(rpn_pytorch[0]['center'].cpu().numpy().mean())
            # print('center_z: ', rel_l2(rpn_onnx[1], rpn_pytorch[0]['center_z'].cpu().numpy()))
            # # print('center_z: ', rel_l2(rpn_onnx[0]['center_z'].cpu().numpy(), rpn_pytorch[0]['center_z'].cpu().numpy()))
            # print(rpn_pytorch[0]['center_z'].cpu().numpy().mean())
            # print('dim: ', rel_l2(rpn_onnx[2], rpn_pytorch[0]['dim'].cpu().numpy()))
            # # print('dim: ', rel_l2(rpn_onnx[0]['dim'].cpu().numpy(), rpn_pytorch[0]['dim'].cpu().numpy()))
            # print(rpn_pytorch[0]['dim'].cpu().numpy().mean())
            # print('rot: ', rel_l2(rpn_onnx[3], rpn_pytorch[0]['rot'].cpu().numpy()))
            # # print('rot: ', rel_l2(rpn_onnx[0]['rot'].cpu().numpy(), rpn_pytorch[0]['rot'].cpu().numpy()))
            # print(rpn_pytorch[0]['rot'].cpu().numpy().mean())
            # print('hm: ', rel_l2(rpn_onnx[4], rpn_pytorch[0]['hm'].cpu().numpy()))
            # # print('hm: ', rel_l2(rpn_onnx[0]['hm'].cpu().numpy(), rpn_pytorch[0]['hm'].cpu().numpy()))
            # print(rpn_pytorch[0]['hm'].cpu().numpy().mean())
            
if __name__ == '__main__':
    main()
