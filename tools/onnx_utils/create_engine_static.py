import os
import numpy as np
import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import trt_utils.inference as inference_utils
import onnxruntime



# def onnx_model_predict(model_dir=None, input_test=None):

#     # input_test = torch.ones([50000, 20, 10], dtype=torch.float32, device="cuda:0")
#     onnx_session = onnxruntime.InferenceSession(model_dir)

#     # Compute ONNX Runtime output prediction
#     onnx_inputs = {onnx_session.get_inputs()[0].name: (input_test)}
#     onnx_outs = onnx_session.run(None, onnx_inputs)
#     return onnx_outs

# def rel_l1(value_torch, value_rt):
#     value_torch = value_torch.reshape(-1)
#     value_rt = value_rt.reshape(-1)
#     rel = np.abs((value_rt - value_torch) / (value_torch + 1e-12))
#     return rel.mean()

# class Config(object):
#     pfe_onnx_path = None
#     rpn_onnx_path = None
#     ext = '.engine' # '.engine' or '.trt'
#     precision = 16 # 16 or 32

# def main(**kwargs):
#     opt = Config()
#     for _k, _v in kwargs.items():
#         setattr(opt, _k, _v)

#     TRT_PRECISION_TO_DATATYPE = {
#         16: trt.DataType.HALF,
#         32: trt.DataType.FLOAT
#     }
#     trt_engine_datatype = TRT_PRECISION_TO_DATATYPE[opt.precision]
#     max_batch_size = 1

#     # create pfe engine
#     trt_inference_wrapper = inference_utils.TRTInference(
#         trt_engine_path=opt.pfe_onnx_path[:-5] + '_static' + opt.ext,
#         onnx_model_path=opt.pfe_onnx_path,
#         trt_engine_datatype=trt_engine_datatype,
#         batch_size=max_batch_size
#     )
#     # compare pfe engine with onnx
#     static_batch = 30000
#     input_data = np.random.randn(static_batch, 32, 10).astype(np.float32)

#     # At runtime you need to set an optimization profile before setting input dimensions. 
#     # trt_inference_wrapper.context.active_optimization_profile = 0
#     # specifying runtime dimensions
#     output_shapes = [(static_batch, 64)]

#     trt_outputs = trt_inference_wrapper.infer(input_data, output_shapes)
#     pfe_onnx = onnx_model_predict(opt.pfe_onnx_path, input_data)
#     pfe_trt = trt_outputs[0]
#     pfe_onnx = pfe_onnx[0].squeeze()
#     # print('pfe tensorrt slices:', pfe_trt[:5, :10])
#     # print('pfe onnx slices:', pfe_onnx[:5, :10])
#     print('pfe relative L1 error: ', rel_l1(pfe_trt, pfe_onnx))

#     # create rpn engine
#     trt_inference_wrapper = inference_utils.TRTInference(
#         trt_engine_path=opt.rpn_onnx_path[:-5] + '_static' + opt.ext,
#         onnx_model_path=opt.rpn_onnx_path,
#         trt_engine_datatype=trt_engine_datatype,
#         batch_size=max_batch_size
#     )
#     # compare pfe engine with onnx
#     static_batch = 1
#     input_data = np.random.randn(static_batch, 64, 640, 320).astype(np.float32)
#     # At runtime you need to set an optimization profile before setting input dimensions. 
#     # trt_inference_wrapper.context.active_optimization_profile = 0
#     # specifying runtime dimensions
#     output_shapes = [(2, 640, 320), (1, 640, 320), (3, 640, 320), (2, 640, 320), (3, 640, 320)]

#     trt_outputs = trt_inference_wrapper.infer(input_data, output_shapes)
#     rpn_onnx = onnx_model_predict(opt.rpn_onnx_path, input_data)
#     rpn_trt = trt_outputs

    
#     print('center L1 error: ', rel_l1(rpn_onnx[0], rpn_trt[0]))
#     print('center_z L1 error: ', rel_l1(rpn_onnx[1], rpn_trt[1]))
#     print('dim L1 error:', rel_l1(rpn_onnx[2], rpn_trt[2]))
#     print('rot L1 error:', rel_l1(rpn_onnx[3], rpn_trt[3]))
#     print('hm L1 error:', rel_l1(rpn_onnx[4], rpn_trt[4]))




# if __name__ == '__main__':
#     pfe_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_pfe_220815.onnx'
#     rpn_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_rpn_220815.onnx'
#     ext = '.engine' # '.engine' or '.trt'
#     precision = 16 # 16 or 32
#     main(pfe_onnx_path=pfe_onnx_path, rpn_onnx_path=rpn_onnx_path, ext=ext, precision=precision)

# # 52062.69 center 
# # L1 error:  4.053188e-05
# # center_z L1 error:  0.0030920897
# # dim L1 error: 0.0006269565
# # rot L1 error: 0.0021324656
# # hm L1 error: 3.9079474e-05



def build_engine(
                    onnx_file_path,
                    engine_file_path,
                    *,
                    use_fp16=True,
                    dynamic_shapes={},
                    dynamic_batch_size=1):
    """Build TensorRT Engine
    :use_fp16: set mixed flop computation if the platform has fp16.
    :dynamic_shapes: {binding_name: (min, opt, max)}, default {} represents not using dynamic.
    :dynamic_batch_size: set it to 1 if use fixed batch size, else using max batch size
    """
    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
    builder = trt.Builder(TRT_LOGGER)
    network = builder.create_network(
        1 << (int)(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH))
    config = builder.create_builder_config()
    config.set_tactic_sources(trt.TacticSource.CUBLAS_LT) # TODO

    # Default workspace is 2G
    config.max_workspace_size = 2 << 30 # TODO

    if builder.platform_has_fast_fp16 and use_fp16:
        config.set_flag(trt.BuilderFlag.FP16)

    # parse ONNX
    parser = trt.OnnxParser(network, TRT_LOGGER)
    with open(onnx_file_path, 'rb') as model:
        if not parser.parse(model.read()):
            print('ERROR: Failed to parse the ONNX file.')
            for error in range(parser.num_errors):
                print(parser.get_error(error))
            return None
    print("===> Completed parsing ONNX file")

    # default = 1 for fixed batch size
    builder.max_batch_size = 1

    if len(dynamic_shapes) > 0:
        print(f"===> using dynamic shapes: {str(dynamic_shapes)}")
        builder.max_batch_size = dynamic_batch_size
        profile = builder.create_optimization_profile()

        for binding_name, dynamic_shape in dynamic_shapes.items():
            min_shape, opt_shape, max_shape = dynamic_shape
            profile.set_shape(
                binding_name, min_shape, opt_shape, max_shape)

        config.add_optimization_profile(profile)

    # Remove existing engine file
    if os.path.isfile(engine_file_path):
        try:
            os.remove(engine_file_path)
        except Exception:
            print(f"Cannot remove existing file: {engine_file_path}")

    print("===> Creating Tensorrt Engine...")
    engine = builder.build_engine(network, config)
    if engine:
        with open(engine_file_path, "wb") as f:
            f.write(engine.serialize())
        print("===> Serialized Engine Saved at: ", engine_file_path)
    else:
        print("===> build engine error")
    return engine

pfe_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_pfe_220815.onnx'
rpn_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_rpn_220815.onnx'
dynamic_shapes = {'input_features': ((1, 32, 10), (7000, 32, 10), (30000, 32, 10))}
build_engine(pfe_onnx_path, pfe_onnx_path[:-5]+'_dynamic.engine', use_fp16=True, dynamic_shapes=dynamic_shapes)