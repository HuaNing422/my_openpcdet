import os
import numpy as np
import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import trt_utils.inference as inference_utils
import onnxruntime



def onnx_model_predict(model_dir=None, input_test=None):

    # input_test = torch.ones([50000, 20, 10], dtype=torch.float32, device="cuda:0")
    onnx_session = onnxruntime.InferenceSession(model_dir)

    # Compute ONNX Runtime output prediction
    onnx_inputs = {onnx_session.get_inputs()[0].name: (input_test)}
    onnx_outs = onnx_session.run(None, onnx_inputs)
    return onnx_outs

def rel_l1(value_torch, value_rt):
    value_torch = value_torch.reshape(-1)
    value_rt = value_rt.reshape(-1)
    rel = np.abs((value_rt - value_torch) / (value_torch + 1e-12))
    return rel.mean()

class Config(object):
    pfe_onnx_path = None
    rpn_onnx_path = None
    ext = '.engine' # '.engine' or '.trt'
    precision = 16 # 16 or 32

def main(**kwargs):
    opt = Config()
    for _k, _v in kwargs.items():
        setattr(opt, _k, _v)

    TRT_PRECISION_TO_DATATYPE = {
        16: trt.DataType.HALF,
        32: trt.DataType.FLOAT
    }
    trt_engine_datatype = TRT_PRECISION_TO_DATATYPE[opt.precision]
    max_batch_size = 1

    # create pfe engine
    dynamic_shapes={'input_features': ((1, 32, 10), (7000, 32, 10), (30000, 32, 10))}
    trt_inference_wrapper = inference_utils.TRTInference(
        trt_engine_path=opt.pfe_onnx_path[:-5] + '_dynamic' + opt.ext,
        onnx_model_path=opt.pfe_onnx_path,
        trt_engine_datatype=trt_engine_datatype,
        batch_size=max_batch_size,
        dynamic_shapes=dynamic_shapes
    )
    # compare pfe engine with onnx
    dynamic_batch = 7338
    input_data = np.random.randn(dynamic_batch, 32, 10).astype(np.float32)

    # At runtime you need to set an optimization profile before setting input dimensions. 
    # trt_inference_wrapper.context.active_optimization_profile = 0
    # specifying runtime dimensions
    trt_inference_wrapper.context.set_binding_shape(0, input_data.shape)
    output_shapes = [(dynamic_batch, 64)]

    trt_outputs = trt_inference_wrapper.infer(input_data, output_shapes)
    pfe_onnx = onnx_model_predict(opt.pfe_onnx_path, input_data)
    pfe_trt = trt_outputs[0]
    pfe_onnx = pfe_onnx[0].squeeze()
    # print('pfe tensorrt slices:', pfe_trt[:5, :10])
    # print('pfe onnx slices:', pfe_onnx[:5, :10])
    print('pfe relative L1 error: ', rel_l1(pfe_trt, pfe_onnx))
    np.save('/home/hning/lidar/my_openpcdet/tools/onnx_utils/mytrt_pfe_fp32.npy', pfe_trt)

    # create rpn engine
    dynamic_shapes={'spatial_features': ((1, 64, 640, 320), (1, 64, 640, 320), (1, 64, 640, 320))}
    trt_inference_wrapper = inference_utils.TRTInference(
        trt_engine_path=opt.rpn_onnx_path[:-5] + '_dynamic'  + opt.ext,
        onnx_model_path=opt.rpn_onnx_path,
        trt_engine_datatype=trt_engine_datatype,
        batch_size=max_batch_size,
        dynamic_shapes=dynamic_shapes
    )
    # compare pfe engine with onnx
    dynamic_batch = 1
    input_data = np.random.randn(dynamic_batch, 64, 640, 320).astype(np.float32)
    # At runtime you need to set an optimization profile before setting input dimensions. 
    # trt_inference_wrapper.context.active_optimization_profile = 0
    # specifying runtime dimensions
    trt_inference_wrapper.context.set_binding_shape(0, input_data.shape)
    output_shapes = [(2, 640, 320), (1, 640, 320), (3, 640, 320), (2, 640, 320), (3, 640, 320)]

    trt_outputs = trt_inference_wrapper.infer(input_data, output_shapes)
    rpn_onnx = onnx_model_predict(opt.rpn_onnx_path, input_data)
    rpn_trt = trt_outputs

    
    print('center L1 error: ', rel_l1(rpn_onnx[0], rpn_trt[0]))
    print('center_z L1 error: ', rel_l1(rpn_onnx[1], rpn_trt[1]))
    print('dim L1 error:', rel_l1(rpn_onnx[2], rpn_trt[2]))
    print('rot L1 error:', rel_l1(rpn_onnx[3], rpn_trt[3]))
    print('hm L1 error:', rel_l1(rpn_onnx[4], rpn_trt[4]))


if __name__ == '__main__':
    pfe_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_pfe_220815.onnx'
    rpn_onnx_path = '/home/hning/lidar/my_openpcdet/tools/onnx_utils/center_rpn_220815.onnx'
    ext = '.engine' # '.engine' or '.trt'
    precision = 16 # 16 or 32
    main(pfe_onnx_path=pfe_onnx_path, rpn_onnx_path=rpn_onnx_path, ext=ext, precision=precision)
