import onnxruntime
import torch
from torch import nn
import numpy as np
from onnx_backbone_2d import BaseBEVBackbone
from onnx_dense_head import  AnchorHeadMulti
from pcdet.config import cfg, cfg_from_yaml_file
import os

def onnx_model_predict(model_dir=None, input_test=None):
    rpn_session = onnxruntime.InferenceSession(model_dir)
    rpn_inputs = {rpn_session.get_inputs()[0].name: (input_test.data.cpu().numpy())}

    rpn_outs = rpn_session.run(None, rpn_inputs)
    print('---------------------- RPN ONNX Outputs ----------------------')
    print(rpn_outs)
    print('---------------------- RPN ONNX Ending ----------------------')

class backbone(nn.Module):
    def __init__(self, cfg , gridx , gridy):
        super().__init__()
        self.backbone_2d = BaseBEVBackbone(cfg.MODEL.BACKBONE_2D, 64)
        self.dense_head =  AnchorHeadMulti(
            model_cfg=cfg.MODEL.DENSE_HEAD,
            input_channels=384,
            num_class=len(cfg.CLASS_NAMES),
            class_names=cfg.CLASS_NAMES,
            grid_size=np.array([gridx , gridy , 1]),
            point_cloud_range=cfg.DATA_CONFIG.POINT_CLOUD_RANGE,
            predict_boxes_when_training=False)

    def forward(self, spatial_features):
        x = self.backbone_2d(spatial_features)
        batch_cls_preds, batch_box_preds = self.dense_head.forward(x)

        return batch_cls_preds, batch_box_preds
    
    def load_params_from_file(self, filename, to_cpu=True):
        if not os.path.isfile(filename):
            raise FileNotFoundError

        print('==> Loading parameters from checkpoint %s to %s' % (filename, 'CPU' if to_cpu else 'GPU'))
        loc_type = torch.device('cpu') if to_cpu else None
        checkpoint = torch.load(filename, map_location=loc_type)
        model_state_disk = checkpoint['model_state']

        if 'version' in checkpoint:
            print('==> Checkpoint trained from version: %s' % checkpoint['version'])

        update_model_state = {}
        for key, val in model_state_disk.items():
            if key in self.state_dict() and self.state_dict()[key].shape == model_state_disk[key].shape:
                update_model_state[key] = val
                # logger.info('Update weight %s: %s' % (key, str(val.shape)))

        state_dict = self.state_dict()
        state_dict.update(update_model_state)
        self.load_state_dict(state_dict)

        for key in state_dict:
            if key not in update_model_state:
                print('Not updated weight %s: %s' % (key, str(state_dict[key].shape)))

        print('==> Done (loaded %d/%d)' % (len(update_model_state), len(self.state_dict())))


def build_backbone_multihead(ckpt , cfg ):

    pc_range = np.array(cfg.DATA_CONFIG.POINT_CLOUD_RANGE)
    voxel_size = np.array(cfg.DATA_CONFIG.DATA_PROCESSOR[2]['VOXEL_SIZE'])
    grid_size = (pc_range[3:] - pc_range[:3]) /voxel_size
    gridx = grid_size[0].astype(np.int)
    gridy = grid_size[1].astype(np.int)
    model = backbone(cfg , gridx ,gridy)
    model.to('cuda').eval()
    model.load_params_from_file(ckpt)
    # checkpoint = torch.load(ckpt, map_location='cuda')
    # dicts = {}
    # for key in checkpoint["model_state"].keys():
    #     if "backbone_2d" in key:
    #         dicts[key] = checkpoint["model_state"][key]
    #     if "dense_head" in key:
    #         dicts[key] = checkpoint["model_state"][key]

    # TODO 这里有bug, 一个不好的解决方法, 把nn.module文件1049行加入error_msgs = []
    # model.load_state_dict(dicts)

    dummy_input = torch.randn(1, 64, gridx, gridy).cuda()
    return model , dummy_input

if __name__ == "__main__":
    cfg_file = '/home/hning/lidar/hova_pcdet/OpenPCDet/tools/cfgs/my_models/my_model_pp_multihead.yaml'
    filename_mh = "/home/hning/lidar/hova_pcdet/OpenPCDet/output/1_my_model_pp_multihead/default/ckpt/checkpoint_epoch_200.pth"
    onnx_mh = '/home/hning/lidar/hova_pcdet/OpenPCDet/output/test_onnx/cbgs_pp_multihead_backbone_2022_3_10.onnx'
    cfg_from_yaml_file(cfg_file, cfg)
    model , dummy_input = build_backbone_multihead(filename_mh , cfg)

    export_onnx_file = "/home/hning/lidar/hova_pcdet/OpenPCDet/output/test_onnx/cbgs_pp_multihead_backbone_2022_3_10.onnx"

    ####### onnx #######
    onnx_model_predict(onnx_mh, dummy_input)
    ####### pytorch #######
    model.eval().cuda()
    print('-------------------------- RPN Pytorch Outputs ----------------------------')
    print(model(dummy_input))
    print('-------------------------- RPN Pytorch Ending ----------------------------')
    
    
